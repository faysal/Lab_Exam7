<?php
namespace App\SummaryOfOrganization;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class SummaryOfOrganization extends DB
{
    public $id="";
    public $user_name="";
    public $organization="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($postVariableData=NULL)
    {
        if (array_key_exists("id",$postVariableData) )
        {
            $this->id = $postVariableData['id'];
        }



        if (array_key_exists("user_name",$postVariableData) )
        {
            $this->user_name = $postVariableData['user_name'];
        }



        if (array_key_exists("organization",$postVariableData) )
        {
            $this->organization = $postVariableData['organization'];
        }
    } // end of set data



    public function store()
    {
        $arryData=array($this->user_name,$this->organization);//secure way...!!
        $sql="insert into summary_of_organization(user_name,organization)VALUES (?, ?)";
        $STH= $this->DBH->prepare($sql);
        $result = $STH->execute($arryData);

        if($result)
            Message::Message("Thank You Data Updated..!");
        else
            Message::Message("Sorry Your Data Save Unsucessful");
        Utility::redirect('create.php');
    }//sql statement store


}// end of book title


