<?php
namespace App\Birthday;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
class Birthday extends DB
{

    public $id="";
    public $user_name="";
    public $birthday="";


    public function setData($postVariableData=NULL)
    {
        if (array_key_exists("id",$postVariableData) )
        {
            $this->id = $postVariableData['id'];
        }



        if (array_key_exists("user_name",$postVariableData) )
        {
            $this->user_name = $postVariableData['user_name'];
        }



        if (array_key_exists("birthday",$postVariableData) )
        {
            $this->birthday = $postVariableData['birthday'];
        }
    } // end of set data



    public function store()
    {
        $arryData=array($this->user_name,$this->birthday);//secure way...!!
        $sql="insert into birthday(user_name,birthday)VALUES (?, ?)";
        $STH= $this->DBH->prepare($sql);
        $result = $STH->execute($arryData);

        if($result)
            Message::Message("Thank You Data Updated..!");
        else
            Message::Message("Sorry Your Data Save Unsucessfull");
        Utility::redirect('create.php');
    }//sql statement store


}// end of book title
