<?php
namespace App\Email;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;

class Email extends DB
{
    public $id="";
    public $user_name="";
    public $user_email="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($postVariableData=NULL)
    {
        if (array_key_exists("id",$postVariableData) )
        {
            $this->id = $postVariableData['id'];
        }



        if (array_key_exists("user_name",$postVariableData) )
        {
            $this->user_name = $postVariableData['user_name'];
        }



        if (array_key_exists("user_email",$postVariableData) )
        {
            $this->user_email = $postVariableData['user_email'];
        }
    } // end of set data



    public function store()
    {
        $arryData=array($this->user_name,$this->user_email);//secure way...!!
        $sql="insert into email(user_name,user_email)VALUES (?, ?)";
        $STH= $this->DBH->prepare($sql);
        $result = $STH->execute($arryData);

        if($result)
            Message::Message("Thank You Data Updated..!");
        else
            Message::Message("Sorry Your Data Save Unsucessful");
        Utility::redirect('create.php');
    }//sql statement store


}// end of book title


